class Ip extends Api {
    constructor(list){
        super();
        this.list = list;
    }
    getIp() {
        return super.get('https://api.ipify.org/?format=json');
    }
    getInfo(ip){
        return super.get2(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district&lang=ru`)
    }
}