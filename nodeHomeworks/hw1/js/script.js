
((UICtrl, RqstCtrl) => {

    const $btnGetMovie = UICtrl.getDOM().$btnGetMovie;
    $btnGetMovie.on('click', function (event) {
        event.preventDefault();

        let doMovsDOM = UICtrl.createMoviesDOM;
        let doCharsDOM = UICtrl.createCharactersDOM;

        RqstCtrl.displayAll(doMovsDOM, doCharsDOM);
    });

})(UserInterfaceController, RequestController);

