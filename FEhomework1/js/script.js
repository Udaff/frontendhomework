let btn = document.getElementById("menuBtn");
let menu = document.getElementsByClassName("header-menu-drop");
let line = document.getElementsByClassName('line1')
btn.addEventListener("click", ()=>{
    if(menu[0].style.display === "block"){
        menu[0].style.display = "none";
        line[1].hidden = false;
        line[2].style.transform = 'none';
        line[0].style.transform = 'none';
    }
    else{
        menu[0].style.display = "block";

        line[1].hidden = true;
        line[2].style.transform = 'rotate(-45deg) translate(1px,-6px)';
        line[0].style.transform = 'rotate(45deg) translate(1px,6px)';
    }
})