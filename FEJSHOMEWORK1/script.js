
/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @param topp        Добавка
* @throws {HamburgerException}  При неправильном использовании
*/
function Hamburger(size, stuffing) {

    this.size = size;
    this.stuffing = stuffing; 
    this.topping = [];
    if(size.cost === undefined && stuffing.cost === undefined) {
        throw new HamburgerException("no size given")
    } else if(size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
        throw new HamburgerException(`invalid size ${this.size.name}`)
    }
 } 

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = { cost: 50, callory: 20, name: "SIZE_SMALL"}
Hamburger.SIZE_LARGE = {cost: 100, callory: 40, name: "SIZE_LARGE" }
Hamburger.STUFFING_CHEESE = { cost: 10, callory: 20, name: "STUFFING_CHEESE" }
Hamburger.STUFFING_SALAD = { cost: 20, callory: 5, name: "STUFFING_SALAD" }
Hamburger.STUFFING_POTATO = { cost: 15, callory: 10, name :"STUFFING_POTATO" }
Hamburger.TOPPING_MAYO = { cost: 20, callory: 5, name: "TOPPING_MAYO" }
Hamburger.TOPPING_SPICE = { cost: 15, callory: 0, name :"TOPPING_SPICE" }

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/

Hamburger.prototype.addTopping = function ( newTopping) {
let currentTopping = newTopping === Hamburger.TOPPING_MAYO ? 'TOPPING_MAYO' : 'TOPPING_SPICE';
    try{
    if (!(this.topping.includes(newTopping))) {
        this.topping.push(newTopping);
    }
} catch(e) {
        throw new HamburgerException(`duplicate topping ${currentTopping}`);
    }
    
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (oldTopping) {
    try{
    let topping = this.topping;
    if (topping.includes(oldTopping)) {
        topping.splice(topping.indexOf(oldTopping), 1);
    }
}catch(e){
    throw new HamburgerException(`not add yet ${oldTopping}`);

}
}
/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    let arr = [];
    this.topping.forEach(element => {
        arr.push(element.name)
    });
    return arr;
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
}
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let fullcost = 0;
    for (let [key, value] of Object.entries(this)) {
        if (value instanceof Array) {
            if (value.length > 1) {
                fullcost += value.reduce((acc, current) => acc.cost + current.cost);
            } else {
                fullcost += value[0].cost;
            }
        } else {
            fullcost += value.cost;
        }
    }
    return fullcost*105.98;
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
        let sum = 0;
        for (let [key, value] of Object.entries(this)) {
            if (value instanceof Array) {
                if (value.length > 1) {
                    sum += value.reduce((acc, current) => acc.callory + current.callory);
                } else {
                    sum += value[0].callory;
                }
            } else {
                sum += value.callory;
            }
        }
        return sum;
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException (message) {
    this.message = message;
    this.name = "Исключение, определенное пользователем";
  }


// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// // я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// // А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// // Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// // Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(hamburger.getToppings());
console.log("Have %d toppings", hamburger.getToppings().length); // 1

// не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger.TOPPING_SAUCE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_MAYO); 
// // HamburgerException: duplicate topping 'TOPPING_MAYO'
